const PORT = process.env.PORT || 8080;
const express = require("express"); //must top from others, else it will cause the data to be persist eventhough have chnag ethe value
const axios = require("axios");
const cheerio = require("cheerio");
const app = express();

//Need to add cors bcs if dont have cors, we couldnt fetch the data
const cors = require("cors");
app.use(cors());

const mangaWebsite = [
  {
    name: "mangapanda.in",
    address: "http://mangapanda.in/manga/",
  },
  { name: "mangaraw.org", address: "https://mangaraw.org/" },
];

let mangaDetails = [];

app.get("/", (req, res) => {
  //   res.send("Hello Express"); // will show like h1 dipslay on the website
  //   res.json("Welcome to my My Manga Searcher Page"); //show in JOSN format

  mangaWebsite.forEach((manga) => {
    const name = "Mikadono Sanshimai Wa Angai, Choroi";
    const chapters = [];

    //1) remove any symbol
    //it will replace everyting except a-z lower & upper, any number and space
    const removeSymbol = name.replace(/[^A-Za-z0-9\s]/g, "");

    //2) turn everyting to lowercase
    const lowercaseName = removeSymbol.toLowerCase();

    //3) add dash to each space
    const nameWithDash = lowercaseName.replaceAll(" ", "-"); // we will add each space with dash to become like this mikadono-sanshimai-wa-angai-choroi

    const mangaPage = manga.address + nameWithDash;

    axios
      .get(mangaPage)
      //for my testing
      //   .get(
      //     // "https://mangapark.net/comic/244195/mikadono-sanshimai-wa-angai-choroi"
      //     "http://mangapanda.in/manga/mikadono-sanshimai-wa-angai-choroi/"
      //   )

      .then((response) => {
        const html = response.data;

        const $ = cheerio.load(html);
        if (manga.name === "mangapanda.in") {
          //only get the a tag with "{name of the manga} Chapter" text
          $(`a:contains("${name} Chapter")`, html).each(function () {
            const title = $(this).text();
            const url = $(this).attr("href");

            chapters.push({
              title,
              url: url,
            });
          });

          //Remove duplicate link and title, so only save one link
          const filteredChapter = [
            ...new Map(
              chapters.map((chapter) => [chapter.url, chapter])
            ).values(),
          ];

          mangaDetails.push({
            source: manga.name,
            chapterDetails: filteredChapter.slice(
              0,
              filteredChapter.length - 2
            ),
          });
        }
        //mangaraw will fail because the website got security that needs user to click 
        else if (manga.name === "mangaraw.org") {
          const mangaChapterHref = mangaPage + "/";
          
          $(`a`, html).each(function () {
            const title = $(this)
              .text()
              .replace(/[^0-9]/g, "");
            const url = $(this).attr("href");

            if (url.includes(mangaChapterHref)) {
              chapters.push({
                title,
                url: url,
              });
            }
          });

          mangaDetails.push({
            source: manga.name,
            chapterDetails: chapters,
          });
        } else {
          console.log("For other than mangapanda and mangaraw");
        }
      })
      .catch((err) => console.log(err));
  });

  res.json(mangaDetails);
});

//For getting certain manga
app.get("/:name", (req, res) => {
  const name = req.params.name;

  mangaWebsite.forEach((manga) => {
    const chapters = [];

    const mangaPage = manga.address + name;

    axios
      .get(mangaPage)

      .then((response) => {
        const html = response.data;

        const $ = cheerio.load(html);
        if (manga.name === "mangapanda.in") {
          //only get the a tag with "{name of the manga} Chapter" text
          $(`a:contains("${name} Chapter")`, html).each(function () {
            const title = $(this).text();
            const url = $(this).attr("href");

            chapters.push({
              title,
              url: url,
            });
          });

          //Remove duplicate link and title, so only save one link
          const filteredChapter = [
            ...new Map(
              chapters.map((chapter) => [chapter.url, chapter])
            ).values(),
          ];

          mangaDetails.push({
            source: manga.name,
            chapterDetails: filteredChapter.slice(
              0,
              filteredChapter.length - 2
            ),
          });
        } else if (manga.name === "mangaraw.org") {
          const mangaChapterHref = mangaPage + "/";

          $(`a`, html).each(function () {
            const title = $(this)
              .text()
              .replace(/[^0-9]/g, "");
            const url = $(this).attr("href");

            if (url.includes(mangaChapterHref)) {
              chapters.push({
                title,
                url: url,
              });
            }
          });

          mangaDetails.push({
            source: manga.name,
            chapterDetails: chapters,
          });
        } else {
          console.log("For other than mangapanda and mangaraw");
        }
      })
      .catch((err) => console.log(err));
  });
  res.json(mangaDetails);
});

// For testing new website whether can test got that page or not
app.get("/test-website", (req, res) => {
  axios
    .get("https://mangaraw.org/mikadono-sanshimai-wa-angai-choroi")
    .then((response) => {
      const html = response.data;
      res.send(html);
    });
  //for my testing
  //   .get(
  //     // "https://mangapark.net/comic/244195/mikadono-sanshimai-wa-angai-choroi" -- error cant pass
  //     "http://mangapanda.in/manga/mikadono-sanshimai-wa-angai-choroi/"
  //   )
});

app.listen(PORT, () => {
  console.log("Server Running on PORT " + PORT);
});
